package org.example.my_maven_array_project;
import  org.example.my_maven_array_projectDemo.Bird;
import org.example.my_maven_array_projectDemo.Flyer;
import  org.example.my_maven_array_projectDemo.Superman;

public class App {
	private Flyer[] flyers;

	public static void main(String[] args) {
		App app = new App();
		app.flyers = new Flyer[10];
		app.flyers[0] = new Bird();
		app.flyers[1] = new Superman();
		app.flyers[0].display();
		app.flyers[1].display();
	}
}
