package org.example.my_maven_array_projectDemo;

public interface Flyer {
	
	public String land();
	public String takeOff();
	public String fly();
	public void display();

}

