package org.example.my_maven_array_projectDemo;

public class Bird implements Flyer {

	@Override
	public String land() {
		// TODO Auto-generated method stub
		return "Bird Landing";
	}

	@Override
	public String takeOff() {
		// TODO Auto-generated method stub
		return "Bird Taking Off";
	}

	@Override
	public String fly() {
		// TODO Auto-generated method stub
		return "Bird Flying";
	}
	
	@Override
	public void display()
	{
		System.out.println(land()+"--"+takeOff()+"--"+fly());
	}

}
